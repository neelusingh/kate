# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-23 01:00+0000\n"
"PO-Revision-Date: 2015-02-01 02:42-0600\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi/Panjabi <punjabi-users@list.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: katefiletree.cpp:120
#, kde-format
msgctxt "@action:inmenu"
msgid "Reloa&d"
msgstr "ਮੁੜ-ਲੋਡ ਕਰੋ(&d)"

#: katefiletree.cpp:122
#, kde-format
msgid "Reload selected document(s) from disk."
msgstr "ਚੁਣੇ ਡੌਕੂਮੈਂਟ ਨੂੰ ਡਿਸਕ ਤੋਂ ਮੁੜ-ਲੋਡ ਕਰੋ"

#: katefiletree.cpp:124
#, kde-format
msgctxt "@action:inmenu"
msgid "Close"
msgstr "ਬੰਦ ਕਰੋ"

#: katefiletree.cpp:126
#, kde-format
msgid "Close the current document."
msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਨੂੰ ਬੰਦ ਕਰੋ।"

#: katefiletree.cpp:128
#, kde-format
msgctxt "@action:inmenu"
msgid "Expand Recursively"
msgstr ""

#: katefiletree.cpp:130
#, kde-format
msgid "Expand the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:132
#, kde-format
msgctxt "@action:inmenu"
msgid "Collapse Recursively"
msgstr ""

#: katefiletree.cpp:134
#, kde-format
msgid "Collapse the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:136
#, kde-format
msgctxt "@action:inmenu"
msgid "Close Other"
msgstr "ਹੋਰ ਬੰਦ ਕਰੋ"

#: katefiletree.cpp:138
#, kde-format
msgid "Close other documents in this folder."
msgstr "ਇਸ ਫੋਲਡਰ ਵਿੱਚ ਹੋਰ ਡੌਕੂਮੈਟਾਂ ਨੂੰ ਬੰਦ ਕਰੋ।"

#: katefiletree.cpp:141
#, fuzzy, kde-format
#| msgctxt "@action:inmenu sorting option"
#| msgid "Opening Order"
msgctxt "@action:inmenu"
msgid "Open Containing Folder"
msgstr "ਖੋਲ੍ਹਣ ਕ੍ਰਮ"

#: katefiletree.cpp:143
#, kde-format
msgid "Open the folder this file is located in."
msgstr ""

#: katefiletree.cpp:145
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Location"
msgstr ""

#: katefiletree.cpp:147
#, fuzzy, kde-format
#| msgid "Copy the filename of the file."
msgid "Copy path and filename to the clipboard."
msgstr "ਫ਼ਾਇਲ ਤੋਂ ਫਾਇਲ-ਨਾਂ ਨੂੰ ਕਾਪੀ ਕਰੋ।"

#: katefiletree.cpp:149
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Rename File"
msgctxt "@action:inmenu"
msgid "Rename..."
msgstr "ਫਾਇਲ ਨਾਂ ਬਦਲੋ"

#: katefiletree.cpp:151
#, kde-format
msgid "Rename the selected file."
msgstr "ਚੁਣੀ ਫਾਇਲ ਦਾ ਨਾਂ ਬਦਲੋ।"

#: katefiletree.cpp:154
#, kde-format
msgid "Print selected document."
msgstr "ਚੁਣੇ ਡੌਕੂਮੈਂਟ ਨੂੰ ਪਰਿੰਟ ਕਰੋ।"

#: katefiletree.cpp:157
#, kde-format
msgid "Show print preview of current document"
msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਲਈ ਪਰਿੰਟ ਝਲਕ ਦਿਖਾਉ"

#: katefiletree.cpp:159
#, fuzzy, kde-format
#| msgid "Delete file?"
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "ਕੀ ਫਾਇਲ ਹਟਾਉਣੀ ਹੈ?"

#: katefiletree.cpp:161
#, kde-format
msgid "Close and delete selected file from storage."
msgstr "ਚੁਣੀ ਗਈ ਫਾਇਲ ਨੂੰ ਬੰਦ ਕਰੋ ਅਤੇ ਸਟੋਰੇਜ਼ ਤੋਂ ਹਟਾ ਦਿਉ।"

#: katefiletree.cpp:165
#, kde-format
msgctxt "@action:inmenu"
msgid "Clear History"
msgstr "ਅਤੀਤ ਸਾਫ਼ ਕਰੋ"

#: katefiletree.cpp:167
#, kde-format
msgid "Clear edit/view history."
msgstr "ਸੋਧ/ਦੇਖਣ ਅਤੀਤ ਨੂੰ ਸਾਫ਼ ਕਰੋ।"

#: katefiletree.cpp:230
#, kde-format
msgctxt "@action:inmenu"
msgid "Tree Mode"
msgstr "ਟਰੀ ਢੰਗ"

#: katefiletree.cpp:231
#, kde-format
msgid "Set view style to Tree Mode"
msgstr ""

#: katefiletree.cpp:237
#, kde-format
msgctxt "@action:inmenu"
msgid "List Mode"
msgstr "ਲਿਸਟ ਢੰਗ"

#: katefiletree.cpp:238
#, kde-format
msgid "Set view style to List Mode"
msgstr ""

#: katefiletree.cpp:245
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Name"
msgstr "ਡੌਕੂਮੈਂਟ ਨਾਂ"

#: katefiletree.cpp:246
#, kde-format
msgid "Sort by Document Name"
msgstr "ਡੌਕੂਮੈਂਟ ਨਾਂ ਰਾਹੀਂ ਲੜੀਬੱਧ ਕਰੋ"

#: katefiletree.cpp:251
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Path"
msgstr "ਡੌਕੂਮੈਂਟ ਪਾਥ"

#: katefiletree.cpp:251
#, kde-format
msgid "Sort by Document Path"
msgstr ""

#: katefiletree.cpp:255
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Opening Order"
msgstr "ਖੋਲ੍ਹਣ ਕ੍ਰਮ"

#: katefiletree.cpp:256
#, kde-format
msgid "Sort by Opening Order"
msgstr "ਖੋਲ੍ਹਣ ਕ੍ਰਮ ਰਾਹੀਂ ਲੜੀਬੱਧ"

#: katefiletree.cpp:259 katefiletreeconfigpage.cpp:74
#, kde-format
msgid "Custom Sorting"
msgstr ""

#: katefiletree.cpp:388
#, kde-format
msgctxt "@action:inmenu"
msgid "Open With"
msgstr "ਇਸ ਨਾਲ ਖੋਲ੍ਹੋ"

#: katefiletree.cpp:402
#, kde-format
msgid "Show File Git History"
msgstr ""

#: katefiletree.cpp:441
#, kde-format
msgctxt "@action:inmenu"
msgid "View Mode"
msgstr "ਦੇਖਣ ਢੰਗ"

#: katefiletree.cpp:445
#, kde-format
msgctxt "@action:inmenu"
msgid "Sort By"
msgstr "ਲੜੀਬੱਧ"

#: katefiletreeconfigpage.cpp:45
#, kde-format
msgid "Background Shading"
msgstr "ਬੈਕਗਰਾਊਂਡ ਸ਼ੇਡਿੰਗ"

#: katefiletreeconfigpage.cpp:52
#, kde-format
msgid "&Viewed documents' shade:"
msgstr ""

#: katefiletreeconfigpage.cpp:58
#, kde-format
msgid "&Modified documents' shade:"
msgstr ""

#: katefiletreeconfigpage.cpp:66
#, kde-format
msgid "&Sort by:"
msgstr "ਲੜੀਬੱਧ(&S):"

#: katefiletreeconfigpage.cpp:71
#, kde-format
msgid "Opening Order"
msgstr ""

#: katefiletreeconfigpage.cpp:72
#, kde-format
msgid "Document Name"
msgstr "ਡੌਕੂਮੈਂਟ ਨਾਂ"

#: katefiletreeconfigpage.cpp:73
#, kde-format
msgid "Url"
msgstr "Url"

#: katefiletreeconfigpage.cpp:79
#, kde-format
msgid "&View Mode:"
msgstr "ਦੇਖਣ ਢੰਗ(&V):"

#: katefiletreeconfigpage.cpp:84
#, kde-format
msgid "Tree View"
msgstr "ਟਰੀ ਝਲਕ"

#: katefiletreeconfigpage.cpp:85
#, kde-format
msgid "List View"
msgstr "ਸੂਚੀ ਝਲਕ"

#: katefiletreeconfigpage.cpp:90
#, kde-format
msgid "&Show Full Path"
msgstr "ਪੂਰਾ ਪਾਥ ਦਿਖਾਉ(&S)"

#: katefiletreeconfigpage.cpp:95
#, kde-format
msgid "Show &Toolbar"
msgstr ""

#: katefiletreeconfigpage.cpp:98
#, kde-format
msgid "Show Close Button"
msgstr ""

#: katefiletreeconfigpage.cpp:100
#, kde-format
msgid ""
"When enabled, this will show a close button for opened documents on hover."
msgstr ""

#: katefiletreeconfigpage.cpp:105
#, kde-format
msgid ""
"When background shading is enabled, documents that have been viewed or "
"edited within the current session will have a shaded background. The most "
"recent documents have the strongest shade."
msgstr ""

#: katefiletreeconfigpage.cpp:108
#, kde-format
msgid "Set the color for shading viewed documents."
msgstr ""

#: katefiletreeconfigpage.cpp:110
#, kde-format
msgid ""
"Set the color for modified documents. This color is blended into the color "
"for viewed files. The most recently edited documents get most of this color."
msgstr ""

#: katefiletreeconfigpage.cpp:115
#, kde-format
msgid ""
"When enabled, in tree mode, top level folders will show up with their full "
"path rather than just the last folder name."
msgstr ""

#: katefiletreeconfigpage.cpp:118
#, kde-format
msgid ""
"When enabled, a toolbar with actions like “Save” are displayed above the "
"list of documents."
msgstr ""

#: katefiletreeconfigpage.cpp:137 katefiletreeplugin.cpp:129
#: katefiletreeplugin.cpp:136
#, kde-format
msgid "Documents"
msgstr "ਡੌਕੂਮੈਂਟ"

#: katefiletreeconfigpage.cpp:142
#, kde-format
msgid "Configure Documents"
msgstr "ਡੌਕੂਮੈਂਟ ਦੀ ਸੰਰਚਨਾ ਕਰੋ"

#: katefiletreemodel.cpp:493
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Open With"
msgctxt ""
"Open here is a description, i.e. 'list of widgets that are open' not a verb"
msgid "Open Widgets"
msgstr "ਇਸ ਨਾਲ ਖੋਲ੍ਹੋ"

#: katefiletreemodel.cpp:647
#, kde-format
msgctxt "%1 is the full path"
msgid ""
"<p><b>%1</b></p><p>The document has been modified by another application.</p>"
msgstr ""

#: katefiletreeplugin.cpp:246
#, kde-format
msgid "Previous Document"
msgstr "ਪਿਛਲਾ ਡੌਕੂਮੈਂਟ"

#: katefiletreeplugin.cpp:252
#, kde-format
msgid "Next Document"
msgstr "ਅਗਲਾ ਡੌਕੂਮੈਂਟ"

#: katefiletreeplugin.cpp:258
#, fuzzy, kde-format
#| msgid "&Show Active"
msgid "&Show Active Document"
msgstr "ਸਰਗਰਮ ਦਿਖਾਉ(&S)"

#: katefiletreeplugin.cpp:263
#, kde-format
msgid "Save"
msgstr ""

#: katefiletreeplugin.cpp:264
#, kde-format
msgid "Save the current document"
msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਨੂੰ ਸੰਭਾਲੋ"

#: katefiletreeplugin.cpp:268
#, kde-format
msgid "Save As"
msgstr ""

#: katefiletreeplugin.cpp:269
#, fuzzy, kde-format
#| msgid "Save current document under new name"
msgid "Save the current document under a new name"
msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਨੂੰ ਨਵੇਂ ਨਾਂ ਨਾਲ ਸੰਭਾਲੋ"

#. i18n: ectx: Menu (go)
#: ui.rc:7
#, kde-format
msgid "&Go"
msgstr ""

#~ msgid "&Other..."
#~ msgstr "ਹੋਰ(&O)..."

#~ msgid "Do you really want to delete file \"%1\" from storage?"
#~ msgstr "ਕੀ ਤੁਸੀਂ \"%1\" ਫਾਇਲ ਨੂੰ ਸਟੋਰੇਜ਼ ਤੋਂ ਹਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ?"

#~ msgid "Delete file?"
#~ msgstr "ਕੀ ਫਾਇਲ ਹਟਾਉਣੀ ਹੈ?"

#~ msgid "File \"%1\" could not be deleted."
#~ msgstr "ਫਾਇਲ \"%1\" ਨੂੰ ਹਟਾਇਆ ਨਹੀਂ ਜਾ ਸਕਿਆ।"

#~ msgid "Rename file"
#~ msgstr "ਫਾਇਲ ਨਾਂ ਬਦਲੋ"

#~ msgid "New file name"
#~ msgstr "ਨਵਾਂ ਫਾਇਲ ਨਾਂ"

#~ msgid "File \"%1\" could not be moved to \"%2\""
#~ msgstr "ਫਾਇਲ \"%1\" ਨੂੰ \"%2\" ਲਈ ਭੇਜਿਆ ਨਹੀਂ ਜਾ ਸਕਿਆ"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅ ਸ ਆਲਮ"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "aalam@users.sf.net"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Close Other"
#~ msgid "Close Widget"
#~ msgstr "ਹੋਰ ਬੰਦ ਕਰੋ"

#~ msgid "Kate File Tree"
#~ msgstr "ਕੇਟ ਫਾਇਲ ਟਰੀ"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Copy Filename"
#~ msgctxt "@action:inmenu"
#~ msgid "Copy File Path"
#~ msgstr "ਫਾਇਲ-ਨਾਂ ਕਾਪੀ ਕਰੋ"

#~ msgid "&View"
#~ msgstr "ਝਲਕ(&V)"

#~ msgid "Save Current Document"
#~ msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਸੰਭਾਲੋ"

#~ msgid "Save Current Document As"
#~ msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਨੂੰ ਇੰਝ ਸੰਭਾਲੋ"

#~ msgid "Application '%1' not found."
#~ msgstr "ਐਪਲੀਕੇਸ਼ਨ '%1' ਨਹੀਂ ਲੱਭੀ।"

#~ msgid "Application not found"
#~ msgstr "ਐਪਲੀਕੇਸ਼ਨ ਨਹੀਂ ਲੱਭੀ"

#~ msgctxt "@action:inmenu"
#~ msgid "Delete Document"
#~ msgstr "ਡੌਕੂਮੈਂਟ ਹਟਾਓ"
