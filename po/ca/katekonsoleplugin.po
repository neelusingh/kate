# Translation of katekonsoleplugin.po to Catalan
# Copyright (C) 2007-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2007, 2008, 2010, 2013, 2017, 2020, 2021.
# Manuel Tortosa <manutortosa@gmail.com>, 2009.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2013, 2014, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-16 01:02+0000\n"
"PO-Revision-Date: 2021-10-29 20:04+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: kateconsole.cpp:54
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"No teniu karma suficient per a accedir a un intèrpret d'ordres o una "
"emulació de terminal"

#: kateconsole.cpp:102 kateconsole.cpp:132 kateconsole.cpp:656
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:141
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Condueix al terminal"

#: kateconsole.cpp:145
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "S&incronitza el terminal amb el document actual"

#: kateconsole.cpp:149
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Executa el document actual"

#: kateconsole.cpp:154 kateconsole.cpp:504
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Mos&tra el plafó del terminal"

#: kateconsole.cpp:160
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr " Dona el &focus al terminal"

#: kateconsole.cpp:373
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Esteu segur que voleu conduir («pipe») el text a la consola? Això executarà "
"qualsevol ordre continguda amb els vostres drets d'usuari."

#: kateconsole.cpp:374
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Condueixo al terminal?"

#: kateconsole.cpp:375
#, kde-format
msgid "Pipe to Terminal"
msgstr "Condueix al terminal"

#: kateconsole.cpp:403
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "No es pot fer «cd» a dins de «%1»"

#: kateconsole.cpp:445
#, kde-format
msgid "Not a local file: '%1'"
msgstr "No és un fitxer local: «%1»"

#: kateconsole.cpp:478
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Esteu segur que voleu executar el document?\n"
"Això executarà l'ordre següent,\n"
"amb els vostres drets, al terminal:\n"
"«%1»"

#: kateconsole.cpp:485
#, kde-format
msgid "Run in Terminal?"
msgstr "Executo en un terminal?"

#: kateconsole.cpp:486
#, kde-format
msgid "Run"
msgstr "Executa"

#: kateconsole.cpp:501
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "O&culta el plafó del terminal"

#: kateconsole.cpp:512
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Treu el focus del plafó del terminal"

#: kateconsole.cpp:513 kateconsole.cpp:514
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Dona el focus al plafó del terminal"

#: kateconsole.cpp:589
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"Sincronitza &automàticament el terminal amb el document actual quan sigui "
"possible"

#: kateconsole.cpp:593 kateconsole.cpp:614
#, kde-format
msgid "Run in terminal"
msgstr "Executa en un terminal"

#: kateconsole.cpp:595
#, kde-format
msgid "&Remove extension"
msgstr "&Elimina l'extensió"

#: kateconsole.cpp:600
#, kde-format
msgid "Prefix:"
msgstr "Prefix:"

#: kateconsole.cpp:608
#, kde-format
msgid "&Show warning next time"
msgstr "Mo&stra un avís la pròxima vegada"

#: kateconsole.cpp:610
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"La pròxima vegada que s'executi «%1», s'assegurà que apareixerà una finestra "
"emergent d'avís, mostrant l'ordre que s'enviarà al terminal, per a revisar-"
"la."

#: kateconsole.cpp:621
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Estableix la variable d'entorn &EDITOR a «kate -b»"

#: kateconsole.cpp:624
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Important: el document s'ha de tancar per a fer que continuï l'aplicació de "
"consola"

#: kateconsole.cpp:627
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Oculta el Konsole en prémer «Esc»"

#: kateconsole.cpp:630
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Això pot causar problemes amb les aplicacions de terminal que fan servir la "
"tecla Esc, p. ex., el vim. Afegiu aquestes aplicacions a continuació a "
"l'entrada (en una llista separada per comes)"

#: kateconsole.cpp:661
#, kde-format
msgid "Terminal Settings"
msgstr "Arranjament del terminal"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Eines"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Josep M. Ferrer"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "txemaq@gmail.com"

#~ msgid "Kate Terminal"
#~ msgstr "Terminal del Kate"

#~ msgid "Terminal Panel"
#~ msgstr "Plafó del terminal"
